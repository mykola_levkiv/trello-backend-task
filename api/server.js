import express from "express";
import { votesController } from "./votes/votes.controller";

export class VotingServer {
  start() {
    this.initServer();
    this.initMiddlewares();
    this.initRoutes();
    this.startListening();
  }

  initServer() {
    this.app = express();
  }

  initMiddlewares() {
    this.app.use(express.json());
  }

  initRoutes() {
    this.app.use("/votes", votesController.get());
  }

  startListening() {
    const { PORT } = process.env;

    this.app.listen(PORT, () => {
      console.log("Started listening on port", PORT);
    });
  }
}
