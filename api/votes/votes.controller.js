import { Router } from "express";
import Joi from "@hapi/joi";

class VotesController {
  constructor() {
    this.router = Router();

    this.votes = [];

    this.router.post(
      "/",
      this.validateMakeVote.bind(this),
      this.makeVotes.bind(this)
    );
    this.router.get("/result", this.getVotingResult.bind(this));
  }

  get() {
    return this.router;
  }

  makeVotes(req, res) {
    const { votes } = req.body;

    this.votes.push(...this._unpackVotes(votes));

    return res.status(204).send();
  }

  getVotingResult(req, res) {
    const votingResult = this._findWinner(this.votes);

    return res.status(200).json(votingResult);
  }

  validateMakeVote(req, res, next) {
    const makeVoteSchema = Joi.object({
      votes: Joi.array().items(Joi.string()).length(3),
    });

    const validationResult = makeVoteSchema.validate(req.body);
    if (validationResult.error) {
      return res.status(400).json(validationResult.error);
    }

    next();
  }

  _unpackVotes(receivedVotes) {
    return receivedVotes.reduce((votes, vote, i) => {
      const convertedVotes = new Array(3 - i).fill(vote);

      return votes.concat(convertedVotes);
    }, []);
  }

  _findWinner(votes) {
    this._validateVotes(votes);

    const votingResults = this._getVotingResults(votes);
    const winner = this._getVotingWinner(votingResults);

    return {
      votes: votingResults,
      winner,
    };
  }

  _validateVotes(votes) {
    const areVotesValid =
      votes && votes.length && votes.every((vote) => typeof vote === "string");

    if (!areVotesValid) {
      throw new Error("Votes should be valid");
    }
  }

  _getVotingResults(votes) {
    return votes.reduce((countPerVote, voteOption) => {
      const votesCount = countPerVote[voteOption] || 0;
      countPerVote[voteOption] = votesCount + 1;

      return countPerVote;
    }, {});
  }

  _getVotingWinner(votingResults) {
    const winnerStats = Object.entries(votingResults).reduce(
      (winner, [vote, count]) => {
        if (count > winner.count) {
          return {
            vote: vote,
            count: count,
          };
        }

        return winner;
      },
      {
        vote: null,
        count: 0,
      }
    );

    return winnerStats.vote;
  }
}

export const votesController = new VotesController();
